const Web3 = require('web3');
const config = require('../config');
const articleJson = require('../contracts/Article.json');

const articleAbi = articleJson.abi;

const web3 = new Web3('http://localhost:7545');

const accountZero = config.accounts[0];

// const { articleAddress } = config;

const article = new web3.eth.Contract(articleAbi);

let articleInstance = {};

function createArticle(title, body) {
  return new Promise((resolve) => {
    resolve();
  });
}

function getRevisionInfo(revisionNumber = '0') {
  return new Promise((resolve) => {
    resolve();
  });
}

const statusEnum = {
  0: 'Null',
  1: 'Draft',
  2: 'Reviewed',
  3: 'Published',
}

function getArticleStatus() {
  return new Promise((resolve) => {
    articleInstance.methods.getArticleStatus().call({ from: accountZero })
      .then((articleStatus) => {
        resolve(statusEnum[articleStatus]);
      })
  })
}

function approveArticle() {
  return new Promise((resolve) => {
    resolve();
  });
}

function rejectArticle(title = 'New title!', body = 'New body', comment = 'This article was bad') {
  return new Promise((resolve) => {
    articleInstance.methods.rejectArticle(
      title,
      body,
      comment
    ).send({ from: config.copyEditorAddress, gas: 5000000 })
      .then((receipt) => {
        resolve(receipt);
      });
  });
}

function approveAsSenior() {
  return new Promise((resolve) => {
    articleInstance.methods.approveAsSenior().send({ from: config.seniorEditorAddress, gas: 5000000 })
      .then((receipt) => {
        resolve(receipt);
      });
  });
}

function rejectAsSenior(title = 'A title!', body = 'A body!', comment = 'This article is not good') {
  return new Promise((resolve) => {
    articleInstance.methods.rejectAsSenior(
      title,
      body,
      comment
    ).send({ from: config.seniorEditorAddress, gas: 5000000 })
      .then((receipt) => {
        resolve(receipt);
      });
  });
}

function reviseArticle(title = 'A title!', body = 'A body!', comment = 'This article is not good.') {
  return new Promise((resolve) => {
    console.log( articleInstance.methods.reviseArticle );
    articleInstance.methods.reviseArticle(title, body, comment)
      .send({ from: config.authorAddress, gas: 5000000 })
      .then((receipt) => {
        resolve(receipt);
      });
  });
}

function getTransferEvents() {
  return new Promise((resolve) => {
    resolve();
  });
}

module.exports = {
  createArticle,
  approveArticle,
  rejectArticle,
  reviseArticle,
  approveAsSenior,
  rejectAsSenior,
  getTransferEvents,
  getRevisionInfo,
  getArticleStatus,
}
