const express = require('express');

const router = express.Router();

const config = require('../config');

const contracts = require('../lib/contracts');

function sendJsonResponse(res, input) {
  res.contentType('application/json');
  res.send(JSON.stringify(input, null, 2));
  res.end();
}

router.get('/createArticle', (req, res) => {
  sendJsonResponse(res, { message: 'Needs to be built'});
});

router.get('/getRevisionInfo', (req, res) => {
  // hard coded parameters
  contracts.getRevisionInfo(req.query.revisionNumber)
    .then((articleInfo) => {
      sendJsonResponse(res, articleInfo);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/getArticleStatus', (req, res) => {
  // hard coded parameters
  contracts.getArticleStatus()
    .then((articleStatus) => {
      sendJsonResponse(res, articleStatus);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/approveArticle', (req, res) => {
  contracts.approveArticle()
    .then((receipt) => {
      sendJsonResponse(res, receipt);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/rejectArticle', (req, res) => {
  contracts.rejectArticle(
    req.query.title,
    req.query.body,
    req.query.comment
  )
    .then((receipt) => {
      sendJsonResponse(res, receipt);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/approveAsSenior', (req, res) => {
  contracts.approveAsSenior()
    .then((receipt) => {
      sendJsonResponse(res, receipt);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/rejectAsSenior', (req, res) => {
  contracts.rejectAsSenior(req.query.title, req.query.body, req.query.comment)
    .then((receipt) => {
      sendJsonResponse(res, receipt);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/reviseArticle', (req, res) => {
  contracts.reviseArticle(
    req.query.title,
    req.query.body,
    req.query.comment
  )
    .then((receipt) => {
      sendJsonResponse(res, receipt);
    })
    .catch((err) => {
      console.log(err);
      sendJsonResponse(res, err);
    });
});

router.get('/getTransferEvents', (req, res) => {
  contracts.getTransferEvents()
    .then((events) => {
      sendJsonResponse(res, events);
    })
    .catch((err) => {
      console.log(err);
    });
});

module.exports = router;
