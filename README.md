This repository is for EY blockchain training (blocktraining)!

This is a series of classes for learning how to develop in Solidity. It assumes a programming background, as well as a familiarity with JavaScript and Node.js

# Requirements

For this course, you will need to download:

[Node.js](https://nodejs.org/en/download/)

Truffle.js (After downloading Node, run `npm install -g truffle`)

[Ganache](http://truffleframework.com/ganache/)

Clone this repo and run npm install in the truffle, api, and src directories respectively.

**For Windows users - You must use Powershell or Git Bash for this project, due to a conflict with how Truffle works**

# Set-up

In the api directory, run `npm start`.

Open Ganache.

In the truffle directory, run `truffle migrate`
