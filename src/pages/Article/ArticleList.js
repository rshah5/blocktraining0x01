import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class ArticleList extends Component {

  constructor( props ){
    super( props );

    this.state = {
      articles: null,
    }
  }

  componentWillMount() {
    console.log('A Fetch to API to retrieve the list of articles goes here.');
  }

  render() {
    return (
      <div>
        <h1>Article List</h1>
        <ReactTable
            manual
            className="-highlight -striped font-small"
            data={ [] }
            columns={ tableColumns }
            loading={ false }
            defaultPageSize={ 10 }
            showPagination={ false }
            />
      </div>
    );
  }
}

let tableColumns = [
  {
    Header: 'Article ID',
    accessor: 'id',
    headerClassName: 'list-header',
    maxWidth: 80,
  },
  {
    Header: 'Title',
    accessor: 'title',
    headerClassName: 'list-header',
  },
  {
    Header: 'Body',
    accessor: 'body',
    headerClassName: 'list-header',
  },
  {
    Header: 'Last Udpated',
    accessor: 'lastUpdated',
    headerClassName: 'list-header',
    maxWidth: 150,
  },
  {
    Header: 'Status',
    accessor: 'status',
    headerClassName: 'list-header',
    maxWidth: 150,
  },
];

export default ArticleList;
