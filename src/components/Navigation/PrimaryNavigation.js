import React, { Component } from 'react';
import {
  Navbar,
  NavItem,
  Nav,
} from 'react-bootstrap';

class PrimaryNavigation extends Component {
  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Solidity Demo</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem href="/article"> Articles </NavItem>
          <NavItem href="/article/new"> New Article </NavItem>
        </Nav>
      </Navbar>
    );
  }
}

export default PrimaryNavigation;
